
%% A semantics for Haskell

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Metavariables  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

metavar x, y, z, f, g, h::= {{ com Term variables }}
metavar a, b ::=            {{ com Type variables }}
metavar K  ::= {{ com Data constructors }}
metavar P  ::= {{ com Pattern synonyms }}
metavar T  ::= {{ com Type constructors }}
indexvar i, j, k, l, n ::=  {{ com Index variables }}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Grammar  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

grammar

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Functions  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

funlhs :: 'FunLhs_' ::= {{ com Function left-hand sides }}
| f ps                 ::   :: Plain           {{ com Standard }}
| f ps '::' c          ::   :: Signature       {{ com Result signature }}

fundecl :: 'FunDecl_' ::= {{ com Function declarations }}
| funeqs                    ::   :: Decl

funeqs {{ tex \overline{\ottnt{funeq} } }} :: 'FunEqs_' ::= {{ com List of function equations }}
| funeq            ::   :: One
| funeqs1 ; .... ; funeqsi  ::   :: Many

funeq :: 'FunEq_' ::= {{ com Function equations }}
| funlhs = e                ::   :: Eqn

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Declarations  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

decl :: 'Decl_' ::= {{ com Declarations }}
| fundecl                   ::   :: FunBind      {{ com Function binding }}
| f '::' c                  ::   :: Sig          {{ com Type signature }}

decls {{ tex \overline{\ottnt{decl} } }} :: 'Decls_' ::= {{ com Lists of declarations }}
| empty           ::  :: Empty
| decl            ::  :: One
| decls1 ; .... ; declsi ::   :: Many

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Expressions  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

CL :: 'ConLike_' ::= {{ com ConLikes }}
| K       ::   :: RealDataCon              {{ com Data constructor }}
| P       ::   :: PatSyn                   {{ com Pattern synonym }}

v {{ tex \nu }} :: 'Atom_' ::= {{ com Atoms }}
| x       :: :: Var          {{ com Variable }}
| CL      :: :: DataCon      {{ com ConLike }}

e :: 'Expr_' ::= {{ com Expressions }}
| v                           ::   :: Atom      {{ com Atom }}
| \ x . e                     ::   :: Lam       {{ com Lambda }}
| \ @ a . e                   ::   :: TyLam     {{ com Type-lambda }}
  {{ tex [[\]]\,[[@]] [[a]] . [[e]] }}
| e1 e2                       ::   :: App       {{ com Application }}
| e @ t                       ::   :: TyApp     {{ com Type application }}
  {{ tex [[e]]\ [[@]][[t]] }}
| case e of { alts }          ::   :: Case      {{ com Pattern-match }}
| ( e )                       ::   :: Parens    {{ com Grouping }}
| let decls in e               ::   :: Let       {{ com Let }}
  {{ tex \keyword{let}\ [[decls]] \, \keyword{in} \, [[e]] }}

alts {{ tex \overline{\ottnt{p} \to \ottnt{e} } }} :: 'Alts_' ::= {{ com Case alternatives }}
| </ pi -> ei // i /> :: :: Patterns

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Patterns  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p :: 'Pat_' ::= {{ com Patterns }}
| _               :: :: Underscore              {{ com Wildcard }}
| x               :: :: Var                     {{ com Variable }}
| p '::' c        :: :: Sig                     {{ com Pattern signature }}
| CL ps           :: M :: Con                   {{ com Constructor }}
| CL atts ps      :: :: ConTys                  {{ com Constructor with type arguments }}
| ( p )           :: :: Parens                  {{ com Grouping }}

ps {{ tex \overline{\ottnt{p} } }} :: 'Pats_' ::= {{ com Lists of patterns }}
| </ pi // i />   :: :: List

atts {{ tex \overline{\at \tau} }} :: 'AtTypes_' ::= {{ com Lists of visible type patterns }}
| @ t1 ... @ ti   ::  :: List

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Types  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

s {{ tex \sigma }} :: 'SigTy_' ::= {{ com Type schemes }}
| Q => s                        ::   :: ForallQTy    {{ com Qualified type }}
| forall { as } . s             ::   :: ForallTy  {{ com Quantified type (inferred) }}
  {{ tex [[forall]] \{ [[as]] \} . \, [[s]] }}
| c                             ::   :: SpecTy  {{ com Specified type scheme }}
| ( s )                         ::   :: Parens  {{ com Grouping }}
| theta s                       :: M :: Subst
  {{ tex [[theta]] [[s]] }}

c {{ tex \chi }} :: 'ChiTy_' ::= {{ com Specified type schemes }}
| forall as . c              ::   :: ForallTy    {{ com Quantified type (specified) }}
| Q => c                     ::   :: ForallQTy   {{ com Qualified type }}
| t                          ::   :: MonoTy      {{ com Monomorphic type }}
| ( c )                      ::   :: Parens      {{ com Grouping }}
| theta c                    :: M :: Subst
  {{ tex [[theta]] [[c]] }}

as {{ tex \overline{\ottmv{a} } }}, bs {{ tex \overline{\ottmv{b} } }} :: 'Vars_' ::= {{ com Lists of inferred variables }}
| </ ai // , // i />         ::   :: List
| as1 , .... , asi           ::   :: Concat

Q, Qr {{ tex Q_r }}, Qp {{ tex Q_p }} :: 'Constraint_' ::= {{ com Constraints }}
| empty                      ::   :: Empty       {{ com Empty }}
| Q1 /\ Q2                   ::   :: Conj        {{ com Conjunction }}
| t1 ~ t2                    ::   :: Eq          {{ com Equality }}
| [ </ ai |-> ti // i /> ] Q :: M :: Subst
| theta Q                    :: M :: SubstVar
  {{ tex [[theta]] [[Q]] }}
| ( Q )                     ::   :: Parens      {{ com Grouping }}
| < Q >                     :: M :: Many
  {{ tex \overline{[[Q]]} }}

t {{ tex \tau }}, u {{ tex \upsilon }} :: 'Ty_' ::= {{ com Monotypes }}
| a                              ::   :: IVar       {{ com Internal variable }}
| s -> t                         ::   :: Fun        {{ com Function }}
| ss -> t                        :: M :: Funs
| T ts                           ::   :: ConApp     {{ com Constructor application }}
| theta t                        :: M :: Subst
  {{ tex [[theta]] [[t]] }}
| ( t )                          ::   :: Parens     {{ com Grouping }}
| Int                            ::   :: Int        {{ com \conid{Int} }}
  {{ tex \conid{Int} }}

ts {{ tex \overline{\tau} }} :: 'Tys_' ::= {{ com Lists of monotypes }}
| </ ti // i />               :: :: List
| as                          :: :: Vars

ss {{ tex \overline{\sigma} }} :: 'Sigmas_' ::= {{ com Lists of polytypes }}
| </ si // , // i />              ::  :: List
| ts                              ::  :: Monotypes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Metatheory  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

theta {{ tex \theta }} :: 'Subst_' ::= {{ com Type substitutions }}
| [ </ ai |-> ti // i /> ]   :: :: SubstList      {{ com Mappings }}
| [< a |-> t >]              :: M :: Overline
  {{ tex [\overline{[[a]] [[|->]] [[t]]}] }}
| theta1 theta2              :: M :: Concat

G {{ tex \Gamma }}, Gt {{ tex \Gamma_t }} :: 'TyEnv_' ::= {{ com Type environments }}
| empty                        :: :: Empty      {{ com Empty }}
| G1 , ... , Gn                :: :: List       {{ com Concatentation }}
| v : s                        :: :: Var        {{ com Variable binding }}
| CL : s                       :: :: PatSyn     {{ com ConLike binding }}
| Q                            :: :: Constraint {{ com Given constraint }}
| a : type                     :: :: TyVar      {{ com Type variable binding }}
| < a : type >                 :: M :: TyVars
  {{ tex \overline{ [[a]] : [[type]] } }}
| ( G )                        :: M :: Parens    {{ com Grouping }}
  {{ tex [[G]] }}
| Giminus_one                  :: M  :: GIHack

tvrel  :: 'TyVarRel_' ::= {{ com Type variable set relation }}
| tvexpr (= tvexpr'             :: :: subset
| tvexpr = tvexpr'              :: :: eq
| tvexpr # tvexpr'              :: :: fresh

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Utility  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

obj :: 'SynObj' ::= {{ com Syntactic object }}
| ss :: :: TypeScheme
| Q  :: :: Constraint
| G  :: :: Context

% This is less typed than it could be
tvexpr :: 'TyVarExpr_' ::= {{ com Type variable set expressions }}
| emptyset                  :: :: Emptyset
| ftv ( obj ) :: :: FTV {{ com Free type variables }}
| dom( G )                  :: :: CtxtDom
  {{ tex \mathsf{dom} ([[G]]) }}
| tvexpr \ tvexpr'          :: :: subtract
  {{ tex [[tvexpr]] \mathop{\backslash} [[tvexpr']] }}
| as                        :: :: VarListNoBraces

num :: 'Num' ::= {{ com Numbers }}
| i               :: :: index
| 1               :: :: one

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Terminals  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

terminals :: 'terminals_' ::=
| \                              ::   :: Lam        {{ tex \lambda }}
| ->                             ::   :: Arrow      {{ tex \to }}
| =>                             ::   :: ThickArrow {{ tex \Rightarrow }}
| |->                            ::   :: Mapsto     {{ tex \mapsto }}
| empty                          ::   :: empty      {{ tex \epsilon }}
| emptyset                       ::   :: emptyset   {{ tex \emptyset }}
| forall                         ::   :: Forall     {{ tex \forall }}
| /\                             ::   :: And        {{ tex \wedge }}
| ~                              ::   :: Eq         {{ tex \sim }}
| |-                             ::   :: VDash1     {{ tex \vdash }}
| ||-                            ::   :: VDash2     {{ tex \Vdash }}
| |-PS                           ::   :: VDashpstar {{ tex \ent{PS} }}
| |-sb*                          ::   :: VDashsbstar {{ tex \ent{sb}^{\!\ast} }}
| :=                             ::   :: Def        {{ tex \coloneqq }}
| in                             ::   :: In         {{ tex \in }}
| '//'                           ::   :: spacer     {{ tex \; }}
| type                           ::   :: type       {{ tex \ast }}
| (=                             ::   :: subseteq   {{ tex \subseteq }}
| without                        ::   :: setminus   {{ tex \setminus }}
| Giminus_one                    ::   :: Giminus_one {{ tex \Gamma_{i-1} }}
| ftv                            ::   :: ftv        {{ tex \mathsf{ftv}\! }}
| #                              ::   :: fresh      {{ tex \mathrel{\#} }}
| @                              ::   :: at         {{ tex \at }}
| -|                             ::   :: dashv      {{ tex \dashv }}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Formulae  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

formula :: formula_ ::=
|  judgement                       ::   :: judgement
|  formula1 '//' .. '//' formulan            ::   :: dots
|  ( v : s ) in G                   ::   :: ctxtlookup
|  ( P : s ) in G                   ::   :: patsynlookup
|  theta1 = theta2                   ::   :: substdef
|  G = G'                           ::   :: ctxtdef
|  tvrel                             ::   :: tvrel
|  obj1 = obj2                       ::   :: objeq
|  < formula >                       ::   :: overline
   {{ tex \overline{[[formula]]} }}
|  num1 > num2                       ::   :: comparison

% | x : t \in G                      ::   :: ctx_in
%   {{ tex [[x]] : [[t]] \in [[G]] }}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Defined functions  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% funs
% Util ::=
%
% fun
% result ( op ) :: t :: arith_result
%   {{ tex [[result]]([[op]]) }}
%
% by
% result(+) === Int
% result(-) === Int
% result(*) === Int
% result(/) === Int
% result(\%) === Int
% result(>) === Bool
% result(>=) === Bool
% result(<) === Bool
% result(<=) === Bool
% result(==) === Bool
%
% fun
% e1 [ e2 / x ] :: e :: theta
%
% by

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Subrules  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subrules


