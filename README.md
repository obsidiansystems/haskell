Typing rules for Haskell
========================

In the course of writing GHC, it frequently comes to pass that we
need proper typing rules for a snippet of Haskell. These will be
written here, using [ott](https://www.cl.cam.ac.uk/~pes20/ott/).

Perhaps someday, this will cover *all* of Haskell. Indeed, doing so
would make a very nice master's level thesis, I would think.

You can access a built version of this PDF at
<https://gitlab.haskell.org/rae/haskell/-/jobs/artifacts/master/raw/haskell.pdf?job=build-pdf>.
